----------------------------------------------------------------------------------
-- Company: FER
-- Engineer: Kristijan Ceple
-- 
-- Create Date:    04:12:49 11/21/2020 
-- Design Name: 
-- Module Name:    pwm01 - RTL 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwm01 is
	port (
			clk: in std_logic;
			rst: in std_logic;
			N : in std_logic_vector(7 downto 0);
			pwm : out std_logic := '0';
			CURR_N : out std_logic_vector(7 downto 0);
			S_CC_debug : out std_logic_vector(7 downto 0)
	);
end pwm01;

architecture RTL of pwm01 is
	signal S_RC : std_logic_vector(7 downto 0) := (others => '0');				-- (S)ignal that connects (R)egister and (C)omparator
	signal S_CC : std_logic_vector(7 downto 0) := (others => '1'); 			-- (S)ignal that connects (C)ounter and (C)omparator
	signal S_AE : std_logic := '1';														-- enabled at the start
begin
	-- D register -- synchronous with a synchronous reset
	process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				S_RC <= (others => '0');
			else
				if S_AE = '1' then
					S_RC <= N;
				end if;
			end if;
			
			CURR_N <= S_RC;
			S_CC_debug <= S_CC;
		end if;
	end process;
	
	-- 8 bit counter -- also synchronous
	process(clk) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				S_CC <= (others => '1');
			else
				S_CC <= S_CC + '1';
			end if;
		end if;
	end process;
	
	-- Comparator -- combinational function, not dependend on clock!
	process(S_RC, S_CC) is
	begin
		if S_RC > S_CC then
			pwm <= '1';
		else
			pwm <= '0';
		end if;
	end process;
	
	-- 8-bit AND between 0xFF and counter output(S_CC)
	process(S_CC) is
	begin
		if S_CC = X"FF" then
			S_AE <= '1';
		else
			S_AE <= '0';
		end if;
	end process;
	
end RTL;

