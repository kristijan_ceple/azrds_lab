--------------------------------------------------------------------------------
-- Company: FER
-- Engineer: Kristijan Ceple
--
-- Create Date:   15:51:43 11/21/2020
-- Design Name:   
-- Module Name:   /media/kikyy_99/HDD/FaksHub/2020_2021/AZRDS/AZRDS_Labs/lab1/ceple_kristijan/projekt01/pwm01_tb.vhd
-- Project Name:  projekt01
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: pwm01
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
-- USE ieee.numeric_std.ALL;
USE ieee.std_logic_arith.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY pwm01_tb IS
END pwm01_tb;
 
ARCHITECTURE behavior OF pwm01_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT pwm01
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         N : IN  std_logic_vector(7 downto 0);
         pwm : OUT  std_logic;
			CURR_N : OUT std_logic_vector(7 downto 0);
			S_CC_debug : OUT std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal N : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal pwm : std_logic;
	signal CURR_N : std_logic_vector(7 downto 0) := N;
	signal S_CC_debug : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 1 us;
	
	-- Verification signal
	signal pwm_active_counter : std_logic_vector(7 downto 0) := (others => '0');
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pwm01 PORT MAP (
          clk => clk,
          rst => rst,
          N => N,
          pwm => pwm,
			 CURR_N => CURR_N,
			 S_CC_debug => S_CC_debug
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 10.5 us.
		rst <= '1';
      wait for 10.5 us;	
		rst <= '0';
		
		-- 2, 7, 10, 157, 255, 256, 128(half), 192(3 quarters), 64(1 quarter), 1591(55 after modulo 256)
		N <= 
			conv_std_logic_vector(1591, 8), 
			"00000010" after 400 us,
			"00000111" after 800 us, 
			"00001010" after 2000 us, 
			"10011101" after 2700 us, "11111111" after 3150 us,
			conv_std_logic_vector(256, 8) after 7500 us, 
			conv_std_logic_vector(128, 8) after 10000 us,
			conv_std_logic_vector(192, 8) after 12500 us,
			conv_std_logic_vector(64, 8) after 15000 us,
			conv_std_logic_vector(1591, 8) after 17500 us;
		
		--wait for 5 us;
		--N <= "00000010";

      -- wait for clk_period*10;
      wait;
   end process;

	-- PWM Verification Counter
	pwm_counter: process(clk, rst, pwm) is
	begin
		if falling_edge(clk) then
			if to_x01(rst) = '1' then
				pwm_active_counter <= (others => '0');
			elsif to_x01(pwm) = '1' then
				pwm_active_counter <= pwm_active_counter + '1';
			elsif to_x01(pwm) = '0' then
				pwm_active_counter <= (others => '0');			-- Either pwm goes to 0, or overflows if 100%. If this counter doesn't reset then multidrive happens and whole line gets X-ed
			end if;
		end if;
	end process;
	
	-- The PWM Verificator itself
	pwm_verificator: process(pwm, rst) is
	begin
		if to_x01(rst) = '0' and falling_edge(pwm) then
			-- Now check if counter is equal to N
			assert pwm_active_counter = CURR_N
				report "PWM ne radi!"
				severity error;
		end if;
	end process;
END;
