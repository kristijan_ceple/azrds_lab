----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    06:17:36 01/14/2021 
-- Design Name: 
-- Module Name:    Counter - CounterArch 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter is
    Port (
		to_add_number : in STD_LOGIC_VECTOR (7 downto 0);
		msb : out STD_LOGIC;
		clk : in STD_LOGIC;
		reset : in STD_LOGIC
		);
end Counter;

architecture CounterArch of Counter is

	signal sig_reg_out : STD_LOGIC_VECTOR(25 downto 0);

begin
	process(clk) is
	begin
		if rising_edge(clk) then
			if To_X01(reset)='1' then
				sig_reg_out <= (others => '0');
			else
				sig_reg_out <= to_add_number + sig_reg_out;
			end if;
			
			msb <= sig_reg_out(25);		-- MSB out
		end if;
	end process;

end CounterArch;

