----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:23:18 01/14/2021 
-- Design Name: 
-- Module Name:    LedStateMachine - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LedStateMachine is
	port(
		clk : in std_logic;
		reset : in std_logic;
		nr: in std_logic;
		trans_enbl: in std_logic;
		ld1, ld2, ld3, ld4: out std_logic
	);
end LedStateMachine;

architecture LSMArch of LedStateMachine is

	type State is (
		Svijetli1, 
		Svijetli2, 
		Svijetli3, 
		Svijetli4, 
		SvijetliSve, 
		SvijetliNista
	);
	signal currentState, nextState: State;

begin

	-- Synchronous State Machine part
	process(clk)
	begin
		if rising_edge(clk) then
			if To_X01(reset) = '1' then
				-- Synchronous reset
				currentState <= Svijetli1;
			else
				currentState <= nextState;
			end if;
		end if;
	end process;
	
	-- Combinational State Machine part
	process(currentState, nr, trans_enbl)
	begin
		ld1 <= '0';
		ld2 <= '0';
		ld3 <= '0';
		ld4 <= '0';
		
		case currentState is
			when Svijetli1 =>
				ld1 <= '1';
				
				if To_X01(trans_enbl) = '1' then
					if To_X01(nr) = '1' then
						nextState <= SvijetliSve;
					else
						nextState <= Svijetli2;
					end if;
				else
					nextState <= Svijetli1;
				end if;
			when Svijetli2 =>
				ld2 <= '1';
				
				if To_X01(trans_enbl) = '1' then
					nextState <= Svijetli3;
				else
					nextState <= Svijetli2;
				end if;
			when Svijetli3 =>
				ld3 <= '1';
				
				if To_X01(trans_enbl) = '1' then
					nextState <= Svijetli4;
				else
					nextState <= Svijetli3;
				end if;
			when Svijetli4 =>
				ld4 <= '1';
				
				if To_X01(trans_enbl) = '1' then
					nextState <= Svijetli1;
				else
					nextState <= Svijetli4;
				end if;
			when SvijetliSve =>
				ld1 <= '1';
				ld2 <= '1';
				ld3 <= '1';
				ld4 <= '1';
				
				if To_X01(trans_enbl) = '1' then
					nextState <= SvijetliNista;
				else
					nextState <= SvijetliSve;
				end if;
			when SvijetliNista =>
				if To_X01(trans_enbl) = '1' then
					if To_X01(nr) = '1' then
						nextState <= Svijetli3;
					else
						nextState <= SvijetliSve;
					end if;
				else
					nextState <= SvijetliNista;
				end if;
			when others =>
				null;
		end case;
	end process;

end LSMArch;

