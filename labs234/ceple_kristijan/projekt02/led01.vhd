----------------------------------------------------------------------------------
-- Company: FER
-- Engineer: Kristijan Ceple
-- 
-- Create Date:    21:43:37 01/13/2021 
-- Design Name: 
-- Module Name:    led01 - RTL 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity led01 is
	port (
		clk	:	in std_logic;
		reset	:	in std_logic;
		ld7	:	out std_logic;
		
		ld6 : out std_logic;
		rot_a, rot_b : in std_logic;
		
		nacin_rada : in std_logic;
		ld1, ld2, ld3, ld4 : out std_logic
	);
end led01;

architecture RTL of led01 is
	component Counter
		port (
			to_add_number : in std_logic_vector(7 downto 0);
			msb : out std_logic;
			clk : in std_logic;
			reset : in std_logic
		);
	end component;
	
	component LEDStateMachine
		port (
			clk : in std_logic;
			reset : in std_logic;
			nr: in std_logic;
			trans_enbl: in std_logic;
			ld1, ld2, ld3, ld4: out std_logic
			);
	end component;
	
	signal sig_synchr_reset: std_logic;
	
	component encoder01
		port (
			clk : in std_logic;
			reset : in std_logic;
			rot_a, rot_b : in std_logic;
			rotary_event, rotary_direction : out std_logic
		);
	end component;
	signal sig_rot_evt : std_logic;
	signal sig_shift_left : std_logic;
	signal sig_shift_enable : std_logic;
	signal sig_shift_reg : std_logic_vector(7 downto 0);
	
	signal sig_nr : std_logic;
	signal sig_pulse_in_msb : std_logic;
	signal sig_pulse_out : std_logic;
	
	signal q1, q2 : std_logic;
	
	signal sig_rot_enc_cnt_mbs_out : std_logic;
begin
	----------------------------------------------------------    
	--                     Vjezba 2    
	----------------------------------------------------------
	-- Flip-flop that remembers the reset
	process(clk) is
	begin
		if rising_edge(clk) then
			sig_synchr_reset <= reset;
		end if;
	end process;
	
	-- LD7 Counter
	LD7_COUNTER: Counter port map (
		reset => sig_synchr_reset,		-- Careful! Flip-flop remembered synchronous reset should be mapped to reset!
		clk => clk,
		to_add_number => "00000001",
		msb => ld7
	);
	
	----------------------------------------------------------    
	--                     Vjezba 3    
	----------------------------------------------------------
	-- Encoder
	ENC01: encoder01 port map(
		clk => clk,
		reset => sig_synchr_reset,
		rot_a => rot_a,
		rot_b => rot_b,
		rotary_event => sig_rot_evt,
		rotary_direction => sig_shift_left
	);
	
	-- LD6 Counter
	LD6_COUNTER : Counter port map (
		reset => sig_synchr_reset,
		clk => clk,
		to_add_number => sig_shift_reg,
		msb => sig_rot_enc_cnt_mbs_out
	);
	ld6 <= sig_rot_enc_cnt_mbs_out;
	
	-- Shift reg
	process(clk)
	begin
		if rising_edge(clk) then
			if To_X01(sig_synchr_reset) = '1' then
				sig_shift_reg <= "00000001";
			else
				if To_X01(sig_shift_enable) = '1' then
					if To_X01(sig_shift_left) = '0' then
						-- Rotation to right
						sig_shift_reg(6 downto 0) <= sig_shift_reg(7 downto 1);
						sig_shift_reg(7) <= '0';
					elsif To_X01(sig_shift_left) = '1' then
						-- Rotation to left
						sig_shift_reg(7 downto 1) <= sig_shift_reg(6 downto 0);
						sig_shift_reg(0) <= '0';
					end if;
				end if;
			end if;
		end if;
	end process;

	-- Combination Function which ensures shift_reg against overflows
	sig_shift_enable <= '1' when 
		(sig_rot_evt = '1') and not( 
			(sig_shift_left = '1' and sig_shift_reg = "10000000") 
			or 
			(sig_shift_left = '0' and sig_shift_reg = "00000001") 
		) 
		else '0';

	----------------------------------------------------------    
	--                     Vjezba 4    
	----------------------------------------------------------
	
	-- nacin_rada flip-flop
	process(clk)
	begin
		if rising_edge(clk) then
			sig_nr <= nacin_rada;
		end if;
	end process;
	
	-- LED Finite State Machine
	LSM: LEDStateMachine port map (
			clk => clk,
			reset => sig_synchr_reset,
			nr => sig_nr,
			trans_enbl => sig_pulse_out,
			ld1 => ld1,
			ld2 => ld2, 
			ld3 => ld3, 
			ld4 => ld4
			);
	
	-- Counter generating ~1Hz period rectangular signal
--	PULSE_CNT: Counter port map (
--		reset => sig_synchr_reset,
--		clk => clk,
--		to_add_number => "00000001",
--		msb => sig_pulse_in_msb
--	);

	sig_pulse_in_msb <= sig_rot_enc_cnt_mbs_out;
	
	-- Pulse module
	PULSE_MDL: process(clk) is
	begin
		if rising_edge(clk) then
			if sig_synchr_reset = '1' then
				q1 <= '0';
				q2 <= '0';
			else
				q1 <= sig_pulse_in_msb;
				q2 <= q1;
			end if;
		end if;
	end process;
	sig_pulse_out <= q1 and (not q2);
	
end RTL;

